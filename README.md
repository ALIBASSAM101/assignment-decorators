# assignment-decorators

## Exercise 1

Take a look at the code named ``PrintInf.py`` and answer the following questions:

1. What does the class ``PrintInf`` do?
2. What is the decorator in this code?
3. What does this decorator do?
4. How you tested this function? Provide the code.

## Exercise 2

We want to create a decorator that counts the numbers of calls. To do so, follow the steps below:

1. Create a class named ``CallCount`` that have two methods: ``__init__`` and ``__call__``.
2. ``__init__`` take one argument and initializes the number of counts. ``__call__`` takes ``* args`` and ``** kwargs`` as input arguments. It increments the number of calls after each call.
3. Outside the class ``CallCount``, create a method called ``hello``, that takes on argument which is the name, and decorate it using ``CallCount``.
4. Test your code to check if the decorator is working as it should be.



1) The class PrintInf has the function  (__call__) that takes one argument (f).
This function has a wrap function that checks first if self.enabled is TRUE.
if self.enabled is TRUE, then it will print the statement('calling' (The address (hash) of the function f in the memory))
Then returns the executed function (f) with its arguments and kwargs. Finaly, (__call__ function) returns the wrap function.


2) The decorator in this code is : @print_inf
It is an instance of the class PrintInf() --> {print_inf = PrintInf()}


3) The decorator will:
* Initialize the class 
* Go to the __Call__ Function   
* Go to the wrap function (Takes any arguments and kwargs) --> check if (self.enabled is true)
* Execute the print statement ('Calling' with the address of the function)
* Then returns the executed function that is in the parameter of rotate_list(.) function.
   
