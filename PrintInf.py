class PrintInf:
    def __init__(self):
        self.enabled = True

    def __call__(self, f):
        def wrap(*args, **kwargs):
            if self.enabled:
                print('Calling {}'.format(f))
            return f(*args, **kwargs)
        return wrap

print_inf = PrintInf()

@print_inf
def rotate_list(l):
    return l[1:] + [l[0]]



class CallCount:
    def __init__(self, City):
         self.Counter = 0
         self.City = City


    def __call__(self, *args, **kwargs):
         self.Counter += 1
         return self.Counter


@CallCount
def hello(City):
    return City


# Test the decorator

print(hello("BEIRUT"))